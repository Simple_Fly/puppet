class dnsresolve {
    exec { 'apt-update':                   
  command => '/usr/bin/apt-get update'
}
file { '/etc/resolv.conf':
  ensure => file,
  content => 'nameserver 8.8.8.8 \n nameserver 8.8.4.4 \n',
  notify  => Exec['apt-update'],
}
}