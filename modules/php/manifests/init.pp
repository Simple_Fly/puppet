class php {
    exec { 'apt-update':                   
  command => '/usr/bin/apt-get update'
}
package { 'php5':
  require => Exec['apt-update'],       
  ensure => installed,
}

file { '/var/www/html/info.php':
  ensure => file,
  content => '<?php  phpinfo(); ?>',        
}
}